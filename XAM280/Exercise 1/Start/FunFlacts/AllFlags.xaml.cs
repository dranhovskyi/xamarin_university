﻿using System;
using System.Collections.Generic;
using FlagData;
using Xamarin.Forms;

namespace FunFlacts
{
    public partial class AllFlags : ContentPage
    {
        private readonly FunFlactsViewModel viewModel;
        private bool isEditing;
        ToolbarItem cancelEditButton;

        public AllFlags()
        {
            InitializeComponent();

            cancelEditButton = (ToolbarItem)Resources[nameof(cancelEditButton)];

            BindingContext = viewModel = DependencyService.Get<FunFlactsViewModel>();
        }

        async void OnDelete(object sender, EventArgs e)
        {
            var s = ((MenuItem)sender);
            var flag = (Flag)s.BindingContext;

            var answer = await DisplayAlert("Delete",
            $"Are you sure you want to delete {flag.Country}?", "Yes", "No");

            if (flag != null && answer)
            {
                DependencyService.Get<FunFlactsViewModel>()
                                   .Flags.Remove(flag);
            }
        }

        async void HandleItemTappedAsync(object sender, ItemTappedEventArgs e)
        {
            if(!isEditing)
                await Navigation.PushAsync(new FlagDetailsPage());
        }

        async void HandleItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (isEditing)
            {
                var flag = (Flag)e.SelectedItem;
                var answer = await DisplayAlert("Delete", 
                $"Are you sure you want to delete {flag.Country}?", "Yes", "No");

                if (flag != null && answer)
                {
                    DependencyService.Get<FunFlactsViewModel>()
                                       .Flags.Remove(flag);
                }

                // Reset the edit button
                OnEdit(cancelEditButton, EventArgs.Empty);
            }
        }

        void OnEdit(object sender, EventArgs e)
        {
            var tbItem = sender as ToolbarItem;
            isEditing = (tbItem == editButton);

            ToolbarItems.Remove(tbItem);
            ToolbarItems.Add(isEditing ? cancelEditButton : editButton);
        }

        private async void HandleRefreshing(object sender, EventArgs e)
        {
            try
            {
                var collection = DependencyService.Get<FunFlactsViewModel>().Flags;
                int i = collection.Count - 1, j = 0;
                while (i > j)
                {
                    var temp = collection[i];
                    collection[i] = collection[j];
                    collection[j] = temp;
                    i--; j++;
                    await System.Threading.Tasks.Task.Delay(200); // make it take some time.
                }
            }
            finally
            {
                // Turn off the refresh.
                ((ListView)sender).IsRefreshing = false;
            }
        }
    }
}
