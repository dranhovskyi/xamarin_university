﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace GreatQuotes.Data
{
    public class QuoteManager
    {
        private static readonly Lazy<QuoteManager> instance = new Lazy<QuoteManager>(() => new QuoteManager());

        private IQuoteLoader loader;

        public static QuoteManager Instance { get { return instance.Value; } }

        public IList<GreatQuote> Quotes { get; private set; }

        private QuoteManager() 
        {
            loader = QuoteLoaderFactory.Create();
            Quotes = new ObservableCollection<GreatQuote>(loader.Load());
        }

        public void Save()
        {
            loader.Save(Quotes); 
        }
    }
}
