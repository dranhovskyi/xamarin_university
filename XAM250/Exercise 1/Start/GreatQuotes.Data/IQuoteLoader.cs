﻿using System;
using System.Collections.Generic;

namespace GreatQuotes.Data
{
    public abstract class IQuoteLoader
    {
        public abstract IEnumerable<GreatQuote> Load();
        public abstract void Save(IEnumerable<GreatQuote> quotes);
    }
}
